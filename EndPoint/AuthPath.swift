//
//  AuthPath.swift
//  TestTextureApp
//
//  Created by Денис Щиголев on 03.03.2021.
//

import Foundation

enum AuthPath: String {
    private var basePath: String { return "auth/" }
    
    case signIn
    case signUp
    
    var path: String {
        switch self {
        case .signIn: return basePath + "login"
        case .signUp: return basePath + "register"
        }
    }
}
