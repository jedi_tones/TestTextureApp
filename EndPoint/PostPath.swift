//
//  PostPath.swift
//  TestTextureApp
//
//  Created by Денис Щиголев on 03.03.2021.
//

import Foundation

enum PostPath: String {
    private var basePath: String { return "posts/" }
    
    case posts
    
    var path: String {
        switch self {
        case .posts: return basePath + "v1/posts"
        }
    }
}
