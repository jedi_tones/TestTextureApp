//
//  PostAPIEndPoint.swift
//  TestTextureApp
//
//  Created by Денис Щиголев on 03.03.2021.
//

import Foundation

enum PostAPIEndPoint {
    case posts(requests: PostRequests)
}

extension PostAPIEndPoint: EndPointType {
    var environment: NetworkEnvironment {
        switch self {
        case .posts: return .stageAPI
        }
    }
    
    var baseURL: URL {
        switch self {
        case .posts: return URL(string: Links.link.rawValue)!
        }
    }
    
    var path: String {
        switch self {
        case .posts: return PostPath.posts.path
        }
    }
    
    var httpMethod: HTTPMethod {
        switch self {
        case .posts: return .get
        }
    }
    
    var task: HTTPTask {
        switch self {
        case .posts(let request):
            let params = JSONEncoder().toBodyParameters(item: request)
            
            return .requestParameters(body: nil, bodyEncoding: .jsonEncoding, urlParameters: params)
        }
    }
    
    var headers: HTTPHeaders? {
        switch self {
        case .posts: return baseHeaders
        }
    }
}
