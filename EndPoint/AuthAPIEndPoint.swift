//
//  AuthAPI.swift
//  TestTextureApp
//
//  Created by Денис Щиголев on 03.03.2021.
//

import Foundation

enum AuthAPIEndPoint: EndPointType {
    case signIn(login: String, password: String)
}

extension AuthAPIEndPoint {
    
    var environment: NetworkEnvironment {
        switch self {
        case .signIn: return .stageAPI
        }
    }
    
    var baseURL: URL {
        switch self {
        case .signIn: return URL(string: Links.link.rawValue)!
        }
    }
    
    var path: String {
        switch self {
        case .signIn: return AuthPath.signIn.path
        }
    }
    
    var httpMethod: HTTPMethod {
        switch self {
        case .signIn: return .post
        }
    }
    
    var task: HTTPTask {
        switch self {
        case .signIn(let login, let password):
            let params: [String: Any] = ["login": login,
                                         "password": password]
            return .requestParameters(body: params, bodyEncoding: .jsonEncoding, urlParameters: nil)
        }
    }
    
    var headers: HTTPHeaders? {
        switch self {
        case .signIn:
            let headers = baseHeaders.merging(baseAnonDeviceModel) { $1 }
            return headers
    
        default:
            return baseHeaders
        }
    }
    
    
}
