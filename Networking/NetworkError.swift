//
//  NetworkError.swift
//  TestTextureApp
//
//  Created by Денис Щиголев on 02.03.2021.
//

import Foundation

enum NetworkError: Error {
    case invalidData
    case parsingJSONFail
    case requestFail
    case incorrectURL
    case badCursorInResponse
    case parametersNil
    case encodingFailed
    case missingURL
}

extension NetworkError: LocalizedError {
    var errorDescription: String? {
        switch self {
        
        case .invalidData:
            return NSLocalizedString("Ошибка получения данных", comment: "")
        case .parsingJSONFail:
            return NSLocalizedString("Ошибка JSON", comment: "")
        case .requestFail:
            return NSLocalizedString("Ошибка запроса", comment: "")
        case .badCursorInResponse:
            return NSLocalizedString("Некорректный курсор в ответе", comment: "")
        case .incorrectURL:
            return NSLocalizedString("Некорректный URL", comment: "")
        case .parametersNil:
            return NSLocalizedString("Parameters were nil", comment: "")
        case .encodingFailed:
            return NSLocalizedString("Parameter encoding failed", comment: "")
        case .missingURL:
            return NSLocalizedString("URL is nil", comment: "")
        }
    }
}
