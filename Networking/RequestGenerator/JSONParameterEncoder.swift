//
//  JSONParameterEncoder.swift
//  TestTextureApp
//
//  Created by Денис Щиголев on 03.03.2021.
//

import Foundation

struct JSONParameterEncoder: ParameterEncoder {
    func encode(urlRequest: inout URLRequest, with parameters: Parameters) throws {
        do {
            guard JSONSerialization.isValidJSONObject(parameters) else {  throw NetworkError.encodingFailed }
            
            let jsonAsData = try JSONSerialization.data(withJSONObject: parameters, options: [])
            urlRequest.httpBody = jsonAsData
            
            if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil &&
                urlRequest.httpBody != nil {
                urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            }
        } catch {
            throw NetworkError.encodingFailed
        }
    }
}

extension JSONEncoder {
    func toBodyParameters<T:Codable>(item: T) -> Parameters {
        guard
            let encode = try? JSONEncoder().encode(item),
            let params = try? JSONSerialization.jsonObject(with: encode, options: []) as? Parameters
            else { return [:] }
        
        return params
    }
}
