//
//  ParameterEncoding.swift
//  TestTextureApp
//
//  Created by Денис Щиголев on 03.03.2021.
//

import Foundation


typealias Parameters = [String:Any]

protocol ParameterEncoder {
    func encode(urlRequest: inout URLRequest, with parameters: Parameters) throws
}

protocol ObjectEncoder {
    func encode(urlRequest: inout URLRequest, with object: Any) throws
}

enum ParameterEncoding {
    case urlEncoding
    case jsonEncoding
    case urlAndJsonEncoding
    
    func encode(urlRequest: inout URLRequest,
                bodyParameters: Any?,
                urlParameters: Parameters?) throws {
        do {
            switch self {
            case .urlEncoding:
                guard let urlParameters = urlParameters else { return }
                try URLParameterEncoder().encode(urlRequest: &urlRequest, with: urlParameters)
                
            case .jsonEncoding:
                guard let bodyParameters = bodyParameters else { return }
                
                if let parameters = bodyParameters as? Parameters {
                    try JSONParameterEncoder().encode(urlRequest: &urlRequest, with: parameters)
                } else {
                    try JSONObjectEncoder().encode(urlRequest: &urlRequest, with: bodyParameters)
                }
                
            case .urlAndJsonEncoding:
                guard let bodyParameters = bodyParameters,
                      let urlParameters = urlParameters else { return }
                try URLParameterEncoder().encode(urlRequest: &urlRequest, with: urlParameters)
                
                if let parameters = bodyParameters as? Parameters {
                    try JSONParameterEncoder().encode(urlRequest: &urlRequest, with: parameters)
                } else {
                    try JSONObjectEncoder().encode(urlRequest: &urlRequest, with: bodyParameters)
                }
                
            }
        } catch {
            throw error
        }
    }
}
