//
//  NetworkRequestConstructor.swift
//  TestTextureApp
//
//  Created by Денис Щиголев on 03.03.2021.
//

import Foundation

class NetworkRequestConstructor {
    let timeoutInterval: Double = 20
    
    func buildRequest(from endPoint: EndPointType) throws -> URLRequest {
        var request = URLRequest(url: endPoint.baseURL.appendingPathComponent(endPoint.path),
                                 timeoutInterval: timeoutInterval)
        
        request.httpMethod = endPoint.httpMethod.rawValue
        do {
            switch endPoint.task {
            case .request:
                if let baseHeaders = endPoint.headers {
                    self.addAdditionalHeaders(baseHeaders, request: &request)
                }
                
            case .requestParameters(let bodyParameters, let bodyEncoding, let urlParameters):
                
                if let baseHeaders = endPoint.headers {
                    self.addAdditionalHeaders(baseHeaders, request: &request)
                }
                
                try self.configureParameters(bodyParameters: bodyParameters,
                                             bodyEncoding: bodyEncoding,
                                             urlParameters: urlParameters,
                                             request: &request)
                
            case .requestParametersAndHeaders(let bodyParameters, let bodyEncoding, let urlParameters, let additionalHeaders):
                
                if let baseHeaders = endPoint.headers {
                    self.addAdditionalHeaders(baseHeaders, request: &request)
                }
                
                self.addAdditionalHeaders(additionalHeaders, request: &request)
                
                try self.configureParameters(bodyParameters: bodyParameters,
                                             bodyEncoding: bodyEncoding,
                                             urlParameters: urlParameters,
                                             request: &request)
                
          
            }
            
            return request
        } catch {
            throw error
        }
    }
    
    fileprivate func configureParameters(bodyParameters: Any?,
                                         bodyEncoding: ParameterEncoding,
                                         urlParameters: Parameters?,
                                         request: inout URLRequest) throws {
        do {
            try bodyEncoding.encode(urlRequest: &request, bodyParameters: bodyParameters, urlParameters: urlParameters)
        } catch {
            throw error
        }
    }
    
    fileprivate func addAdditionalHeaders(_ additionalHeaders: HTTPHeaders?, request: inout URLRequest) {
        guard let headers = additionalHeaders else { return }
        for (key, value) in headers {
            request.setValue(value, forHTTPHeaderField: key)
        }
    }
}
