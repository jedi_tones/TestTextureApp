//
//  JSONObjectDecoder.swift
//  TestTextureApp
//
//  Created by Денис Щиголев on 03.03.2021.
//

import Foundation

struct JSONObjectEncoder: ObjectEncoder {
    func encode(urlRequest: inout URLRequest, with object: Any) throws {
        do {
            guard JSONSerialization.isValidJSONObject(object) else {  throw NetworkError.encodingFailed }
            
            let jsonAsData = try JSONSerialization.data(withJSONObject: object, options: [])
            urlRequest.httpBody = jsonAsData
            
            if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil &&
                urlRequest.httpBody != nil {
                urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            }
        } catch {
            throw NetworkError.encodingFailed
        }
    }
}

