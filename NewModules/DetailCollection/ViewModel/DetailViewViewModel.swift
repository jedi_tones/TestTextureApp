//
//  DetailViewViewModel.swift
//  TestTextureApp
//
//  Created by Денис Щиголев on 02.03.2021.
//

import Foundation

protocol DetailViewViewModelProtocol {
    var imageURL: URL? { get set }
    var text: String { get set }
}

struct DetailViewViewModel: DetailViewViewModelProtocol {
    var imageURL: URL? = nil
    var text = ""
}
