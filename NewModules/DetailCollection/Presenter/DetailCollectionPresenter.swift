//
//  DetailCollectionDetailCollectionPresenter.swift
//  TestTextureApp
//
//  Created by Denis Shchigolev on 02/03/2021.
//  Copyright © 2021 jedi-tones. All rights reserved.
//

class DetailCollectionPresenter: DetailCollectionModuleInput, DetailCollectionViewOutput, DetailCollectionInteractorOutput {

    weak var view: DetailCollectionViewInput!
    var interactor: DetailCollectionInteractorInput!
    var router: DetailCollectionRouterInput!
    var model: DetailViewViewModelProtocol!
    
    func viewIsReady() {
        guard let model  = self.model as? DetailViewViewModel else { fatalError() }
        view.setupInitialState()
        view.setupData(model: model)
    }
}
