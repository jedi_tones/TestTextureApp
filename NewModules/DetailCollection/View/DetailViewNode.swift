//
//  DetailViewNode.swift
//  TestTextureApp
//
//  Created by Денис Щиголев on 02.03.2021.
//

import AsyncDisplayKit

class DetailViewNode: ASDisplayNode {
    
    let imageNode = ASNetworkImageNode()
    let textNode = ASTextNode()
    let scrollNode = ASScrollNode()
   
    override init() {
        super.init()
        
        setup()
    }
    
    private func setup() {
        
        automaticallyManagesSubnodes = true
        textNode.maximumNumberOfLines = 0
        scrollNode.automaticallyManagesSubnodes = true
        scrollNode.automaticallyManagesContentSize = true

        scrollNode.layoutSpecBlock = {[unowned self] node, constrainedSize in
            
            var childrens: [ASLayoutElement] = []
            let mainVerticalStack = ASStackLayoutSpec.vertical()
            mainVerticalStack.spacing = 10
            
            let textSpec = ASInsetLayoutSpec(insets: .init(top: .zero,
                                                           left: 10,
                                                           bottom: .zero,
                                                           right: 10),
                                             child: textNode)
            
            childrens.append(textSpec)
            childrens.append(imageNode)
            
            mainVerticalStack.children?.append(contentsOf: childrens)
            
            return ASInsetLayoutSpec(insets: .init(top: 10,
                                                   left: 10,
                                                   bottom: 10,
                                                   right: 10),
                                     child: mainVerticalStack)
        }
        
        view.backgroundColor = .white
    }
    
    func configure(model: DetailViewViewModel) {
        
        imageNode.url = model.imageURL
        
        print(model)
        let paragraphStyle: NSMutableParagraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.left
        
        let attributes: [NSAttributedString.Key : Any] = [
            .foregroundColor : UIColor.label,
            .paragraphStyle : paragraphStyle
        ]
        
        textNode.attributedText = NSAttributedString(string: model.text,
                                                     attributes: attributes)
        
    }
    
    
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        
        return ASInsetLayoutSpec(insets: .zero,
                                 child: scrollNode)
    }
}
