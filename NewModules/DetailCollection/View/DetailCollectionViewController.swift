//
//  DetailCollectionDetailCollectionViewController.swift
//  TestTextureApp
//
//  Created by Denis Shchigolev on 02/03/2021.
//  Copyright © 2021 jedi-tones. All rights reserved.
//

import UIKit
import AsyncDisplayKit
import TinyConstraints

class DetailCollectionViewController: UIViewController, DetailCollectionViewInput {

    var output: DetailCollectionViewOutput!
    var detailNode = DetailViewNode()
    
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        output.viewIsReady()
    }


    // MARK: DetailCollectionViewInput
    func setupInitialState() {
        
        setup()
        setupConstraints()
        
    }
    
    func setup() {
        view.backgroundColor = .white
    }
    
    func setupData(model: DetailViewViewModel) {
        detailNode.configure(model: model)
    }
    
   
    private func setupConstraints() {
        view.addSubnode(detailNode)
        
        detailNode.view.edgesToSuperview(usingSafeArea: true)
    }
}
