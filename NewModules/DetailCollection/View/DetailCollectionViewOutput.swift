//
//  DetailCollectionDetailCollectionViewOutput.swift
//  TestTextureApp
//
//  Created by Denis Shchigolev on 02/03/2021.
//  Copyright © 2021 jedi-tones. All rights reserved.
//

protocol DetailCollectionViewOutput {

    /**
        @author Denis Shchigolev
        Notify presenter that view is ready
    */

    func viewIsReady()
}
