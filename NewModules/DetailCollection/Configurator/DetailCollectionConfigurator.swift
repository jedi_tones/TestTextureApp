//
//  DetailCollectionDetailCollectionConfigurator.swift
//  TestTextureApp
//
//  Created by Denis Shchigolev on 02/03/2021.
//  Copyright © 2021 jedi-tones. All rights reserved.
//

import UIKit

class DetailCollectionModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController, model: DetailViewViewModelProtocol) {

        if let viewController = viewInput as? DetailCollectionViewController {
            configure(viewController: viewController, model: model)
            
        }
    }

    private func configure(viewController: DetailCollectionViewController, model: DetailViewViewModelProtocol) {

        let router = DetailCollectionRouter()

        let presenter = DetailCollectionPresenter()
        presenter.view = viewController
        presenter.router = router
        presenter.model = model

        let interactor = DetailCollectionInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
