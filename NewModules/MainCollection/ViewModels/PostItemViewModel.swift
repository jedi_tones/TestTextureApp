//
//  PostItemViewModel.swift
//  TestTextureApp
//
//  Created by Денис Щиголев on 02.03.2021.
//

import UIKit

protocol PostItemViewModelProtocol {
    var imageURL: URL? { get set }
    var text: String { get set }
}

struct PostItemViewModel: PostItemViewModelProtocol {
    var imageURL: URL? = nil
    
    var text: String = ""
}
