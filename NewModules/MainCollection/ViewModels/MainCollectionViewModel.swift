//
//  MainCollectionViewModel.swift
//  TestTextureApp
//
//  Created by Денис Щиголев on 02.03.2021.
//

import UIKit

class MainCollectionViewModel: CellViewModel {

    var id: UUID = UUID()
    var cell: ConfigurableCell.Type {
        return PostCellNode.self
    }
    
    private(set) var post: PostItemViewModel
    
    var imageURL: URL? {
        return post.imageURL
    }
    
    var text: String {
        return post.text
    }
    
    init(_ post: PostItemViewModel) {
        self.post = post
    }
}


