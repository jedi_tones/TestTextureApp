//
//  MainCollectionMainCollectionConfigurator.swift
//  TestTextureApp
//
//  Created by Denis Shchigolev on 02/03/2021.
//  Copyright © 2021 jedi-tones. All rights reserved.
//

import UIKit

class MainCollectionModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? MainCollectionViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: MainCollectionViewController) {

        let router = MainCollectionRouter()
        router.transitionHandeler = viewController
        
        let presenter = MainCollectionPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = MainCollectionInteractor()
        interactor.output = presenter
        interactor.networkService = NetworkService()
        interactor.requestConstructor = NetworkRequestConstructor()
        
        presenter.interactor = interactor
        viewController.output = presenter
    }

}
