//
//  MainCollectionMainCollectionPresenter.swift
//  TestTextureApp
//
//  Created by Denis Shchigolev on 02/03/2021.
//  Copyright © 2021 jedi-tones. All rights reserved.
//
import Foundation

class MainCollectionPresenter: MainCollectionModuleInput, MainCollectionViewOutput, MainCollectionInteractorOutput {

    weak var view: MainCollectionViewInput!
    var interactor: MainCollectionInteractorInput!
    var router: MainCollectionRouterInput!
    
    private var cursor: String?
    private var entity: DataModel?
    private var items: [Item] = []
    private var mainCollectionViewModels: [MainCollectionViewModel] = []
    private var sortedType = DataSortType.createdAt
    
    func viewIsReady() {
        view.setupInitialState()
        interactor.getPosts(cursor: cursor, sortedType: sortedType)
    }
    
    func changeItems(newEntity: DataModel) {
        entity = newEntity
        cursor = newEntity.data.cursor
        items.append(contentsOf: newEntity.data.items)
        
        mainCollectionViewModels = mainCollectionViewModelGenerator(items: items)
        view.setPosts(items: mainCollectionViewModels)
    }
    
    func cellTappedAt(index: Int) {
        guard mainCollectionViewModels.count > index else { return }
        
        let viewModel = detailViewViewModelGenerator(item: mainCollectionViewModels[index])
        router.showDetailViewController(model: viewModel)
    }
    
    
    private func detailViewViewModelGenerator(item: MainCollectionViewModel) -> DetailViewViewModel {
        return  DetailViewViewModel(imageURL: item.imageURL,
                                    text: item.text)
    }
    
    private func mainCollectionViewModelGenerator(items: [Item]) -> [MainCollectionViewModel] {
        let viewModels = items.map { (item) -> MainCollectionViewModel in
            
            var imageURL:URL?
            var itemText = ""
            
            //setup main content data
            item.contents.forEach { content in
                switch content.type {
                
                case .image:
                    guard let stringURL = content.data.original?.url,
                          let url = URL(string: stringURL) else { break }
                    imageURL = url

                case .text:
                    guard let text = content.data.value else { break }
                    itemText = text
                default:
                    break
                }
            }
            
            let post = PostItemViewModel(imageURL: imageURL, text: itemText)
            
            let viewModel = MainCollectionViewModel(post)
            
            return viewModel
        }
        
        return viewModels
    }
}
