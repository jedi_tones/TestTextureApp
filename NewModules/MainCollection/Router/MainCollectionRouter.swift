//
//  MainCollectionMainCollectionRouter.swift
//  TestTextureApp
//
//  Created by Denis Shchigolev on 02/03/2021.
//  Copyright © 2021 jedi-tones. All rights reserved.
//

import UIKit

class MainCollectionRouter: MainCollectionRouterInput {
    
    var transitionHandeler: UIViewController!
    
    func showDetailViewController(model: DetailViewViewModelProtocol) {
        let configurator = DetailCollectionModuleConfigurator()
        let vc = DetailCollectionViewController()
        
        configurator.configureModuleForViewInput(viewInput: vc,
                                                 model: model)
        
        transitionHandeler.navigationController?.pushViewController(vc, animated: true)
    }
}
