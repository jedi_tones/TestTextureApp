//
//  MainCollectionMainCollectionRouterInput.swift
//  TestTextureApp
//
//  Created by Denis Shchigolev on 02/03/2021.
//  Copyright © 2021 jedi-tones. All rights reserved.
//

import Foundation

protocol MainCollectionRouterInput {

    func showDetailViewController(model: DetailViewViewModelProtocol)
}
