//
//  MainCollectionMainCollectionInteractor.swift
//  TestTextureApp
//
//  Created by Denis Shchigolev on 02/03/2021.
//  Copyright © 2021 jedi-tones. All rights reserved.
//

class MainCollectionInteractor: MainCollectionInteractorInput {

    weak var output: MainCollectionInteractorOutput!
    var networkService: NetworkService!
    var requestConstructor: NetworkRequestConstructor!
    
    func getPosts(cursor: String?, sortedType: DataSortType) {
        
        
        let postRequest = PostRequests(orderBy: sortedType.rawValue,
                                       first: 10,
                                       after: cursor)
        if let request = try? requestConstructor.buildRequest(from: PostAPIEndPoint.posts(requests: postRequest)) {
            
            networkService.createDataTask(request: request) {[unowned self] (result: Result<DataModel, NetworkError>) in
                switch result {
                
                case .success(let entity):
                    output.changeItems(newEntity: entity)
                case .failure(let error):
                    print("error \(error.localizedDescription)")
                }
            }
        }
        
        guard let requestLogin = try? requestConstructor.buildRequest(from: AuthAPIEndPoint.signIn(login: "jediTones",
                                                                                              password: "Password10")) else { return }
        networkService.createDataTask(request: requestLogin) { (result: Result<AnonAuth, NetworkError>) in
            switch result {
            
            case .success(let authResult):
                print("\n \(authResult) \n")
            case .failure(let error):
                print("\n error: \(error.localizedDescription) \n")
            }
        }
    }
}
