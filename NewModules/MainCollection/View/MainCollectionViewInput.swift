//
//  MainCollectionMainCollectionViewInput.swift
//  TestTextureApp
//
//  Created by Denis Shchigolev on 02/03/2021.
//  Copyright © 2021 jedi-tones. All rights reserved.
//

protocol MainCollectionViewInput: class {

    /**
        @author Denis Shchigolev
        Setup initial state of the view
    */

    func setupInitialState()
    
    func setPosts(items:[MainCollectionViewModel])
}
