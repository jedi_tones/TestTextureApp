//
//  MainCollectionMainCollectionViewController.swift
//  TestTextureApp
//
//  Created by Denis Shchigolev on 02/03/2021.
//  Copyright © 2021 jedi-tones. All rights reserved.
//

import UIKit
import TinyConstraints

class MainCollectionViewController: UIViewController {

    var output: MainCollectionViewOutput!
    var collectionNode = MainCollectionNode(collectionViewFlowLayout: UICollectionViewFlowLayout())

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }


    // MARK: MainCollectionViewInput
    func setupInitialState() {
        collectionNode.dataDelegate = self
        setupConstraints()
    }
  
    func setupConstraints() {
        view.addSubnode(collectionNode)
        collectionNode.view.edgesToSuperview()
    }
    
}

extension MainCollectionViewController: MainCollectionNodeDelegate {
    func cellTouch(at indexPath: IndexPath) {
        output.cellTappedAt(index: indexPath.row)
    }
}

extension MainCollectionViewController: MainCollectionViewInput {
    func setPosts(items:[MainCollectionViewModel]) {
        collectionNode.setData(items: items)
        print(items)
    }
}
