//
//  MainCollectionNodeDelegate.swift
//  TestTextureApp
//
//  Created by Денис Щиголев on 02.03.2021.
//

import Foundation

protocol MainCollectionNodeDelegate {
    func cellTouch(at indexPath: IndexPath)
    
}
