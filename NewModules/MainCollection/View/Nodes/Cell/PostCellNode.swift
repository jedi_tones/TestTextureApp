//
//  PostCellNode.swift
//  TestTextureApp
//
//  Created by Денис Щиголев on 02.03.2021.
//

import Foundation
import AsyncDisplayKit

class PostCellNode: ASCellNode {
    var viewModel: MainCollectionViewModel?
    
    private let imageNode: ASNetworkImageNode
    private let textNode: ASTextNode2
    
    private let textFont: UIFont = .systemFont(ofSize: 16)
    
    private var needImage: Bool {
        return viewModel?.imageURL != nil
    }
    
    required override init() {
        imageNode = ASNetworkImageNode()
        textNode = ASTextNode2()
        
        super.init()
        
        automaticallyManagesSubnodes = true
        
        textNode.truncationMode = .byWordWrapping
        textNode.maximumNumberOfLines = 5
    }
    
  
    private func configure() {
        guard let viewModel = viewModel else { return }
        
        if needImage {
            imageNode.url = viewModel.imageURL
        }
        
        updateTitle()
        
        cornerRadius = 5
    }
    
    private func updateTitle() {
        guard let viewModel = viewModel else { return }
        
        let paragraphStyle: NSMutableParagraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.left
        
        let attributes: [NSAttributedString.Key : Any] = [
            .foregroundColor : UIColor.label,
            .paragraphStyle : paragraphStyle
        ]
        
        textNode.attributedText = NSAttributedString(string: viewModel.text,
                                                     attributes: attributes)
    }
    
    override func didLoad() {
        super.didLoad()
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let verticalMainStack = ASStackLayoutSpec.vertical()
        verticalMainStack.alignItems = .center
        verticalMainStack.spacing = 10
        
        var childrens: [ASLayoutSpec] = []
        
        let textSpec = ASInsetLayoutSpec(insets: .zero, child: textNode)
   
        childrens.append(textSpec)
        
        
        
        imageNode.style.preferredSize = CGSize(width: constrainedSize.max.width,
                                               height: constrainedSize.max.width)
        
        if needImage {
            childrens.append(ASInsetLayoutSpec(insets: .zero, child: imageNode))
        }
        
        verticalMainStack.children = childrens
        
        return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 10,
                                                      left: 10,
                                                      bottom: 10,
                                                      right: 10), child: verticalMainStack)
    }
}

extension PostCellNode: ConfigurableCell {
    
    func configure(_ viewModel: CellViewModel?) -> Self {
        if let viewModel = viewModel as? MainCollectionViewModel {
            self.viewModel = viewModel
        }
        
        configure()
        
        return self
    }
    
}
