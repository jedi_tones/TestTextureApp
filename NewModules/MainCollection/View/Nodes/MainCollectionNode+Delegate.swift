//
//  MainCollectionNode+Delegate.swift
//  TestTextureApp
//
//  Created by Денис Щиголев on 02.03.2021.
//

import AsyncDisplayKit

extension MainCollectionNode: ASCollectionDelegate {
    
    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        dataDelegate?.cellTouch(at: indexPath)
    }
    
  
}
