//
//  MainCollectionNode.swift
//  TestTextureApp
//
//  Created by Денис Щиголев on 02.03.2021.
//

import UIKit
import AsyncDisplayKit

class MainCollectionNode: ASCollectionNode {
   
    var didScroll: ((_ scrollView: UIScrollView) -> ())?
    
    var data: [MainCollectionViewModel] = []
    var dataDelegate: MainCollectionNodeDelegate?
    
    init(collectionViewFlowLayout: UICollectionViewFlowLayout) {
        super.init(frame: .zero,
                   collectionViewLayout: collectionViewFlowLayout,
                   layoutFacilitator: nil)
        
        setupViews()
    }
    
    func setData(items: [MainCollectionViewModel]) {
        
        updateData(newData: items )
    }
    
    private func setupViews() {
        
        delegate = self
        dataSource = self
        
        showsVerticalScrollIndicator = false
        showsHorizontalScrollIndicator = false
        
        setIntelligentPreloading()
    }
    
    private func setIntelligentPreloading() {
        var preloadTuning = ASRangeTuningParameters()
        preloadTuning.leadingBufferScreenfuls = 2
        preloadTuning.trailingBufferScreenfuls = 1
        
        setTuningParameters(preloadTuning, for: .preload)
        
        var displayTuning = ASRangeTuningParameters()
        displayTuning.leadingBufferScreenfuls = 1
        displayTuning.trailingBufferScreenfuls = 0.5
        
        setTuningParameters(displayTuning, for: .display)
        
        leadingScreensForBatching = 2
    }
    
    @objc private func handleRefreshControl() {
        //dataDelegate?.refreshRequest()
    }
    
    private func updateData(newData: [MainCollectionViewModel]) {
       
        DispatchQueue.main.async {
            self.data = newData
            self.reloadData()
        }
    }
}
