//
//  MainCollectionNode+DataSource.swift
//  TestTextureApp
//
//  Created by Денис Щиголев on 02.03.2021.
//

import Foundation

import AsyncDisplayKit

extension MainCollectionNode: ASCollectionDataSource {
    
    func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
        1
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        data.count
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let node = getCellNode(at: indexPath)
        
        let nodeBlock: ASCellNodeBlock = {
            return node
        }
        
        return nodeBlock
    }
    
    private func getCellNode(at indexPath: IndexPath) -> ASCellNode {
        let vm = data[indexPath.row]
        let cell = vm.cell.init()
        cell.configure(vm)
        return cell as? ASCellNode ?? ASCellNode()
    }
}
