//
//  CoordinatorProtocol.swift
//  TestTextureApp
//
//  Created by Денис Щиголев on 02.03.2021.
//

import UIKit

protocol CoordinatorProtocol {
    func mainCollection() -> UIViewController
}
