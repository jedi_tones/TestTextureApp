//
//  Coordinator.swift
//  TestTextureApp
//
//  Created by Денис Щиголев on 02.03.2021.
//

import UIKit

class Coordinator: CoordinatorProtocol {
    
    func mainCollection() -> UIViewController {
        let configurator = MainCollectionModuleConfigurator()
        let vc = MainCollectionViewController()
        configurator.configureModuleForViewInput(viewInput: vc)
        
        let navController = configureNavController(rootVC: vc)
        return navController
    }
  
}

extension Coordinator {
    
    private func configureNavController(rootVC: UIViewController) -> UINavigationController {
        
        let navigationController = UINavigationController(rootViewController: rootVC)
        
        let appereance = navigationController.navigationBar.standardAppearance.copy()
        
        appereance.shadowImage = UIImage()
        appereance.shadowColor = .clear
        appereance.backgroundImage = UIImage()
        appereance.backgroundColor = .systemBackground
        
        navigationController.navigationBar.standardAppearance = appereance
        navigationController.navigationBar.tintColor = .label
        navigationController.navigationBar.prefersLargeTitles = false
        
        navigationController.navigationBar.titleTextAttributes = [.font: UIFont.systemFont(ofSize: 16)]
        navigationController.navigationBar.largeTitleTextAttributes = [.font: UIFont.systemFont(ofSize: 32)]
        
        return navigationController
    }
}
