//
//  AnonAuth.swift
//  TestTextureApp
//
//  Created by Денис Щиголев on 03.03.2021.
//

import Foundation

struct AnonAuth: Codable {
    let data: AnonAuthData
}

struct AnonAuthData: Codable {
    let id: String
    let login: String?
    var email: String?
    
    let rocketId: String?
    let rocketKey: String?
    
    let isDisabled: Bool?
    let disabledUntil: Int?
    //let disableReason: AnonAuthBlock?
    
    let token: String?
    
    //let roles: [String]
    let level: Int?
}
