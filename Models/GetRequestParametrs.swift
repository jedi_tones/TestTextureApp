//
//  GetRequestParametrs.swift
//  TestTextureApp
//
//  Created by Денис Щиголев on 02.03.2021.
//

import Foundation

import Foundation

enum GetRequestParametrs: String {
    case first
    case after
    case orderBy
}
