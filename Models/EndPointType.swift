//
//  EndPointType.swift
//  TestTextureApp
//
//  Created by Денис Щиголев on 03.03.2021.
//

import UIKit

enum NetworkEnvironment {
    case publicAPI
    case devAPI
    case stageAPI
    case oldAPI
}

protocol EndPointType {
    var environment: NetworkEnvironment { get }
    var baseURL: URL { get }
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var task: HTTPTask { get }
    var headers: HTTPHeaders? { get }
}

extension EndPointType {
    var request: URLRequest? {
        do {
            let networkRequestConstructor = NetworkRequestConstructor()
            let urlRequest = try networkRequestConstructor.buildRequest(from: self)
            return urlRequest
        }
        catch (let error) {
            print(error)
        }
        return nil
    }
}

//MARK: - requestUrl
extension EndPointType {
    var requestUrl: URL? {
        return request?.url
    }
}

//MARK: - baseHeaders
extension EndPointType {
    var baseHeaders: [String:String] {
        get {
            var headers: [String:String] = [:]
            
            headers["Accept-Language"] = "RU"
            headers["X-Country-Code"] = "RU"
            headers["X-Device-Id"] = String(describing: UIDevice.current.identifierForVendor!)
            headers["X-Application-Version"] = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String) ?? "Empty"
            
            return headers
        }
    }
    
    var baseAuthHeaders: [String:String] {
        var base = baseHeaders
        
      
            base["Authorization"] = "authToken"
        
        
        return base
    }
    
    var baseAnonDeviceModel: [String:String] {
        return ["X-Device-Model": "deviceModel"]
    }
}
