//
//  PostRequests.swift
//  TestTextureApp
//
//  Created by Денис Щиголев on 03.03.2021.
//

import Foundation

struct PostRequests: Codable {
    var orderBy: String?
    var first: Int?
    var after: String?
}

