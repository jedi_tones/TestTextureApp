//
//  CellViewModel.swift
//  TestTextureApp
//
//  Created by Денис Щиголев on 02.03.2021.
//

import AsyncDisplayKit
import Foundation

protocol CellViewModel {
    var id: UUID { get }
    
    var cell: ConfigurableCell.Type { get }
}

protocol ConfigurableCell: AnyObject {
    @discardableResult func configure(_ viewModel: CellViewModel?) -> Self

    init()
}
