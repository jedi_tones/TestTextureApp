//
//  Links.swift
//  TestTextureApp
//
//  Created by Денис Щиголев on 02.03.2021.
//

import Foundation

enum Links: String {
    case link = "https://k8s-stage.apianon.ru"
    case postLink = "https://k8s-stage.apianon.ru/posts/v1/posts"
}
