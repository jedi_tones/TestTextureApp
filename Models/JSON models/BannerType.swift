//
//  BannerType.swift
//  TestTask
//
//  Created by Денис Щиголев on 27.01.2021.
//

import Foundation

enum BannerType: String, Decodable {
    case text = "TEXT"
    case image = "IMAGE"
    case video = "VIDEO"
    case tags = "TAGS"
    case audio = "AUDIO"
}
