//
//  LinkGenerator.swift
//  TestTextureApp
//
//  Created by Денис Щиголев on 02.03.2021.
//

import Foundation

final class LinkGenerator {
    static let shared = LinkGenerator()
    private init() {}
    
    func getMainLink(itemsCount:Int, cursor: String?, sortType: DataSortType) -> String {
        
        let link = Links.postLink.rawValue +
            "?\(GetRequestParametrs.first.rawValue)=\(itemsCount)" +
            "&\(GetRequestParametrs.after.rawValue)=\(cursor ?? "")" +
            "&\(GetRequestParametrs.orderBy.rawValue)=\(sortType.rawValue)"
        print(link)
        return link
    }
}
